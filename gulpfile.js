var gulp = require('gulp'),
    sass = require('gulp-sass'),
    php = require('gulp-connect-php'),
    browserSync = require('browser-sync');
    //imagemin = require('gulp-imagemin'),
    //cache = require('gulp-cache');

var src = {
    allJS: 'src/**/*.js',
    allCss: './public/css/*.css',
    allScss: './public/sass/**/*.scss',
    scss:  'public/sass/main.scss',
    allImages: './public/img/**/*.+(png|jpg|gif|svg)'
};

var dest = {
    css:   './public/css',
    image: './dist/img'
};

gulp.task('images', function(){
  return gulp.src(src.allImages)
  .pipe(cache(imagemin({
    interlaced: false
  })))
  .pipe(gulp.dest(dest.image))
});

gulp.task('sass', function(){
  return gulp.src(src.scss)
    .pipe(sass()) // Using gulp-sass
    .pipe(gulp.dest(dest.css))
    .pipe(browserSync.reload({
      stream: true
    }))
});

gulp.task('php', function() {
    php.server({ base: 'public', port: 8010, keepalive: true});
});

gulp.task('browserSync', function() {
    browserSync({
        proxy: '127.0.0.1:8010',
        port: 8080,
        open: true,
        notify: false
    });
})

gulp.task('browserSync:html', function() {
  browserSync({
    server: {
      baseDir: 'public'
    },
  })
})

gulp.task('watch', function (){
    gulp.watch(src.allScss, ['sass']);
    
    // Reloads the browser whenever HTML/PHP or JS files change
    //gulp.watch('public/*.html', browserSync.reload); 
    gulp.watch('public/*.php', browserSync.reload); 
    //gulp.watch('public/js/**/*.js', browserSync.reload); 
});

gulp.task('server', ['sass', 'php', 'browserSync', 'watch']);

gulp.task('default', ['server']);
