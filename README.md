# Gulp PHP SASS Workflow

HTML/CSS/JS developement workflow for PHP website.

References
* [PHP Browsersync](http://fettblog.eu/php-browsersync-grunt-gulp/)
* [Gulp for Beginners](https://css-tricks.com/gulp-for-beginners/)

Requirements
------------
* [Node.js](https://nodejs.org/en/) & npm
* gulp

Installation
------------
Install `gulp` package globally. See [Fix Priviliges](http://studiorgb.uk/fix-priviliges-and-never-again-use-sudo-with-npm/) for without `sudo` with npm.
```
npm install -g gulp
```
Install dependencies
```
cd <project_folder>
npm install
```

Usage
------------
```
gulp
```